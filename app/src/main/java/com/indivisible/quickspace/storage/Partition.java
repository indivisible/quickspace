package com.indivisible.quickspace.storage;

import java.io.File;
import android.content.Context;
import android.util.Log;
import com.indivisible.quickspace.R;


public class Partition
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    public PartitionType partitionType = PartitionType.UNSET;
    private File partitionRoot;
    private String partitionTitle;
    private long spaceTotal;
    private long spaceFree;
    private long spaceUsed;

    private static final String TAG = "Partition";


    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    /** class to hold stats on a single storage partition/partitionType **/
    public Partition(File partitionRoot, PartitionType partitionType, Context context)
    {
        this.partitionRoot = partitionRoot;
        this.partitionType = partitionType;
        initTitle(context);
        updateStats();
    }

    /**
     * set the partition's partitionTitle depending on storage type and
     * location
     **/
    private void initTitle(Context context)
    {
        switch (this.partitionType)
        {
            case INTERNAL:
                partitionTitle = context.getString(R.string.storage_title_internal);
                break;
            case INTERNAL_SDCARD:
                partitionTitle = context.getString(R.string.storage_title_internal_sdcard);
                break;
            case EXTERNAL_SDCARD:
                partitionTitle = context.getString(R.string.partition_title_external_sdcard);
                break;
            case TERTIARY:
                // has String formatting
                partitionTitle = context.getString(R.string.partition_title_tertiary,
                        partitionRoot.getName());
                break;
            case UNSET:
                partitionTitle = context.getString(R.string.partition_title_unset);
                break;
            default:
                Log.e(TAG, "(initTitle) Unhandled PartitionType: " + this.partitionType.name());
                Log.e(TAG, "(initTitle) Path: " + partitionRoot.getAbsolutePath());
                // use same title as Unknown
            case UNKNOWN:
                partitionTitle = context.getString(R.string.partition_title_unknown);
                break;
        }
    }


    ///////////////////////////////////////////////////////
    ////    gets & update
    ///////////////////////////////////////////////////////


    /** update all storage stats. use when reloading **/
    public void updateStats()
    {
        spaceTotal = partitionRoot.getTotalSpace();
        spaceFree = partitionRoot.getFreeSpace();
        spaceUsed = spaceTotal - spaceFree;
    }

    /** return storage folder partitionTitle **/
    public String getPartitionTitle()
    {
        return partitionTitle;
    }

    /** get the total space allocated to this partition in bytes **/
    public long getSpaceTotal()
    {
        return spaceTotal;
    }

    /** get the amount of space available on the partition in bytes **/
    public long getSpaceFree()
    {
        return spaceFree;
    }

    /** get the amount of used space on the partition in bytes **/
    public long getSpaceUsed()
    {
        return spaceUsed;
    }

    /** calculate and return the percentage of free space remaining **/
    public float getPercentageFree()
    {
        return ((float) spaceFree / (float) spaceTotal) * 100f;
    }

    /** calculate and return the percentage of space occupied **/
    public float getPercentageUsed()
    {
        return ((float) spaceUsed / (float) spaceTotal) * 100f;
    }

    ///////////////////////////////////////////////////////
    ////    util
    ///////////////////////////////////////////////////////


    /** convert bytes to gigabytes (long to float) **/
    public static float convBytesToGigs(long bytes)
    {
        return bytes / 1024f / 1024f / 1024f;
    }

}
