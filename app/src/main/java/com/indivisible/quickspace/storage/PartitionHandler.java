package com.indivisible.quickspace.storage;


import java.io.File;
import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import android.os.Environment;
import android.util.Log;

/** class to contain and parse all storage partitions **/
public class PartitionHandler
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private ArrayList<Partition> allPartitions;

    private static final String TAG = "PartitionHandler";


    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    /** class to contain and parse all storage partitions **/
    public PartitionHandler(Context context)
    {
        allPartitions = new ArrayList<Partition>();
        addNativeStorage(context);
        addTertiaryStorage(context);
    }


    ///////////////////////////////////////////////////////
    ////    collect storage locations
    ///////////////////////////////////////////////////////


    /** append native storage to the List **/
    private void addNativeStorage(Context context)
    {
        // internal storage
        File internal = Environment.getRootDirectory();
        allPartitions.add(new Partition(internal, PartitionType.INTERNAL, context));

        // default external storage. can be either internal (emulated) or external (sdCard)
        File externalStorage = Environment.getExternalStorageDirectory();
        if (Environment.isExternalStorageRemovable())
        {
            allPartitions.add(new Partition(externalStorage, PartitionType.EXTERNAL_SDCARD,
                    context));
        }
        else
        {
            allPartitions.add(new Partition(externalStorage, PartitionType.INTERNAL_SDCARD,
                    context));
        }
    }

    /** append any tertiary storage to the List **/
    private void addTertiaryStorage(Context context)
    {
        String[] roots = RenzhiStorage.getStorageDirectories();
        for (String root : roots)
        {
            File rootDir = new File(root);
            if (rootDir.canRead())
            {
                allPartitions.add(new Partition(rootDir, PartitionType.TERTIARY, context));
            }
            else
            {
                Log.w(TAG, "(Tertiary) Can't read: " + rootDir.getAbsolutePath());
            }
        }
    }


    ///////////////////////////////////////////////////////
    ////    list management
    ///////////////////////////////////////////////////////


    /** update all store's stats **/
    public void update()
    {
        for (Partition store : allPartitions)
        {
            store.updateStats();
        }
    }

    /** get all storage partitions **/
    public List<Partition> getAllPartitions()
    {
        return allPartitions;
    }


    ///////////////////////////////////////////////////////
    ////    storage tests
    ///////////////////////////////////////////////////////


    //TODO: implement partition tests
    // /** test standard external storage for readability **/
    // private boolean isExternalStorageReadable()
    // {
    // String state = Environment.getExternalStorageState();
    // if (Environment.MEDIA_MOUNTED.equals(state) ||
    // Environment.MEDIA_MOUNTED_READ_ONLY.equals(state))
    // {
    // return true;
    // }
    // Log.w(TAG, "External Partition unreadable:");
    // Log.w(TAG, state);
    // return false;
    // }


    ///////////////////////////////////////////////////////
    ////    util
    ///////////////////////////////////////////////////////

    /**
     * display stats for all stores Depreciated: part of old Toast
     * notification service (MainService.java)
     **/
    @Deprecated
    public String disp()
    {
        StringBuffer sb = new StringBuffer();

        boolean isFirst = true;
        for (Partition store : allPartitions)
        {
            if (isFirst)
            {
                sb.append(store.toString());
                isFirst = false;
            }
            else
            {
                sb.append("\n").append(store.toString());
            }
        }

        return sb.toString();
    }


}
