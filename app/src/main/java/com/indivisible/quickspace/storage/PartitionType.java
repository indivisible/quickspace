package com.indivisible.quickspace.storage;


public enum PartitionType
{

    INTERNAL,           // main storage, OS installed here
    INTERNAL_SDCARD,    // emulated secondary storage
    EXTERNAL_SDCARD,    // physical secondary storage
    TERTIARY,           // other. could be supplemental secondary storage or partition
    UNSET,              // default value, actual has not yet been assigned
    UNKNOWN;            // error occurred somewhere
}
