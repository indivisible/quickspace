package com.indivisible.quickspace.storage;

import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import cn.limc.androidcharts.entity.TitleValueColorEntity;
import cn.limc.androidcharts.view.PieChart;
import com.indivisible.quickspace.R;

/** Custom ArrayAdapter for displaying stats on partitions **/
public class PartitionArrayAdapter
        extends ArrayAdapter<Partition>
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private Context context;
    private List<Partition> partitions;

    private static String TAG = "PartArrayAdapter";


    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////


    /** Custom ArrayAdapter for displaying stats on partitions **/
    public PartitionArrayAdapter(Context ctx, int layout, List<Partition> stores)
    {
        super(ctx, layout, stores);
        partitions = stores;
        context = ctx;
    }


    ///////////////////////////////////////////////////////
    ////    populate
    ///////////////////////////////////////////////////////

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View view = convertView;
        if (view == null)
        {
            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.row_partition, null);
        }

        PieChart pieChart = (PieChart) view.findViewById(R.id.row_limcPieChart);
        TextView title = (TextView) view.findViewById(R.id.row_title);
        TextView spaceFree = (TextView) view.findViewById(R.id.row_freeGigs);
        TextView percentFree = (TextView) view.findViewById(R.id.row_freePercent);
        TextView spaceTotal = (TextView) view.findViewById(R.id.row_total_space);

        Partition partition = partitions.get(position);
        if (partition != null)
        {
            // pie chart
            configurePieChart(partition, pieChart);

            // standard views
            title.setText(partition.getPartitionTitle());
            String printableSpaceFree = printableGigs(Partition.convBytesToGigs(partition
                    .getSpaceFree()));
            spaceFree.setText(printableSpaceFree);
            String printablePercentageFree = printPercentage(partition.getPercentageFree());
            percentFree.setText(printablePercentageFree);
            String printableSpaceTotal = printableGigs(Partition.convBytesToGigs(partition
                    .getSpaceTotal()));
            spaceTotal.setText(context.getString(R.string.row_space_total_text,
                    printableSpaceTotal));
        }
        else
        {
            Log.d(TAG, "(getView) Partition was null");
            title.setText("Invalid partition");
        }
        return view;
    }


    ///////////////////////////////////////////////////////
    ////    util
    ///////////////////////////////////////////////////////

    private PieChart configurePieChart(Partition partition, PieChart pieChart)
    {
        float spaceFreeValue = (float) partition.getSpaceFree() / partition.getSpaceTotal();
        float spaceUsedValue = (float) partition.getSpaceUsed() / partition.getSpaceTotal();
        // REM: I disabled text dram in PieChart.java
        //        String spaceFreeGigabytes = context.getString(R.string.format_gigabytes,
        //                (float) partition.getSpaceFree());
        //        String spaceUsedPercentage = context.getString(R.string.format_percentage,
        //                (float) partition.getPercentageUsed());
        String spaceFreeGigabytes = "";
        String spaceUsedPercentage = "";

        List<TitleValueColorEntity> pieData = new ArrayList<TitleValueColorEntity>();
        pieData.add(new TitleValueColorEntity(spaceFreeGigabytes, spaceFreeValue, context
                .getResources().getColor(R.color.color_freeSpace)));
        pieData.add(new TitleValueColorEntity(spaceUsedPercentage, spaceUsedValue, context
                .getResources().getColor(R.color.color_usedSpace)));
        pieChart.setData(pieData);

        pieChart.setDisplayBorder(false);
        pieChart.setCircleBorderColor(context.getResources()
                .getColor(R.color.color_transparent));
        return pieChart;
    }

    /** util method to convert gigabyte floats to printable text **/
    private String printableGigs(float gigs)
    {
        return context.getString(R.string.format_gigabytes, gigs);
    }

    /**
     * util method to convert a float representation of a percentage to
     * printable text
     **/
    private String printPercentage(float percentage)
    {
        return context.getString(R.string.format_percentage, percentage);
    }

    public int dpToPx(int dp)
    {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }


}
