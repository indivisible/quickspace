package com.indivisible.quickspace.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import com.indivisible.quickspace.R;

/**
 * Created by indiv on 29/06/14.
 */
public class TitlebarFragment
        extends Fragment
        implements View.OnClickListener
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private ImageButton prefsButton;
    private ImageButton infoButton;

    private boolean showButtons = false;
    private TitlebarButtonClickCallback buttonCallback;

    private static final String KEY_SHOW_BUTTONS = "do_show_buttons";
    private static final String TAG = "TitlebarFrag";


    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    public static final TitlebarFragment newInstance(boolean showButtons)
    {
        Bundle args = new Bundle(1);
        args.putBoolean(KEY_SHOW_BUTTONS, showButtons);
        TitlebarFragment frag = new TitlebarFragment();
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_titlebar, container, false);
        prefsButton = (ImageButton) view.findViewById(R.id.title_button_preferences);
        infoButton = (ImageButton) view.findViewById(R.id.title_button_info);

        Bundle args = getArguments();
        if (args != null)
        {
            showButtons = args.getBoolean(KEY_SHOW_BUTTONS);
        }
        if (showButtons)
        {
            prefsButton.setOnClickListener(this);
            infoButton.setOnClickListener(this);
        }
        else
        {
            prefsButton.setVisibility(View.GONE);
            infoButton.setVisibility(View.GONE);
        }
        return view;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try
        {
            buttonCallback = (TitlebarButtonClickCallback) activity;
        }
        catch (ClassCastException e)
        {
            Log.e(TAG,
                    "(onAttach) parent Activity must implement 'TitlebarButtonClickCallback'");
            throw e;
        }
    }

    ///////////////////////////////////////////////////////
    ////    communication
    ///////////////////////////////////////////////////////

    public interface TitlebarButtonClickCallback
    {

        public void onPrefsButtonClick();

        public void onInfoButtonClick();
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.title_button_preferences:
                Log.d(TAG, "(click) prefs");
                buttonCallback.onPrefsButtonClick();
                break;
            case R.id.title_button_info:
                Log.d(TAG, "(click) info");
                buttonCallback.onInfoButtonClick();
                break;
        }
    }

}
