package com.indivisible.quickspace.fragment;

import java.util.List;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import com.indivisible.quickspace.R;
import com.indivisible.quickspace.storage.PartitionArrayAdapter;
import com.indivisible.quickspace.storage.Partition;
import com.indivisible.quickspace.storage.PartitionHandler;


public class PartitionListFragment
        extends ListFragment
        implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private PartitionHandler partitionHandler;
    private PartitionArrayAdapter adapter;

    private static final String TAG = "PartitionListView";


    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    public static final PartitionListFragment newInstance()
    {
        PartitionListFragment frag = new PartitionListFragment();

        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        Log.d(TAG, "(onCreateView) start inflate");
        if (partitionHandler == null)
        {
            partitionHandler = new PartitionHandler(getActivity());
        }
        List<Partition> partitions = partitionHandler.getAllPartitions();
        Log.i(TAG, "(onCreateView) Partitions found: " + partitions.size());
        adapter = new PartitionArrayAdapter(getActivity(), R.layout.row_partition, partitions);
        setListAdapter(adapter);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        getListView().setOnItemClickListener(this);
        getListView().setOnItemLongClickListener(this);
    }

    ///////////////////////////////////////////////////////
    ////    click handling
    ///////////////////////////////////////////////////////

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        Log.v(TAG, "(shortClick) item: " + adapter.getItem(position).getPartitionTitle());
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
    {
        Log.v(TAG, "(longClick) item: " + adapter.getItem(position).getPartitionTitle());
        return true;
        //return false;
    }
}
