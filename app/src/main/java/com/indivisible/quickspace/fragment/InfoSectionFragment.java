package com.indivisible.quickspace.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.indivisible.quickspace.R;

/**
 * Created by indiv on 25/06/14.
 */
public class InfoSectionFragment
        extends Fragment
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private static final String KEY_TITLE = "section_title";
    private static final String KEY_BODY = "section_body";

    private static final String TAG = "InfoSectFrag";

    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    public static final InfoSectionFragment newInstance(String sectionTitle,
                                                        String sectionBody,
                                                        Integer... formatResources)
    {
        InfoSectionFragment frag = new InfoSectionFragment();
        Bundle args = new Bundle(2);
        args.putString(KEY_TITLE, sectionTitle);
        args.putString(KEY_BODY, sectionBody);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        // gather resources
        Bundle args = getArguments();
        if (args == null)
        {
            Log.e(TAG, "(onCreateView) args is null!!");
        }
        String sectionTitleText = args.getString(KEY_TITLE);
        String sectionBodyText = args.getString(KEY_BODY);

        // assign views
        View view = inflater.inflate(R.layout.fragment_info_section, container, false);
        TextView sectionTitle = (TextView) view.findViewById(R.id.info_section_title_text);
        TextView sectionBody = (TextView) view.findViewById(R.id.info_section_body_text);
        sectionTitle.setText(sectionTitleText);
        sectionBody.setText(Html.fromHtml(sectionBodyText), TextView.BufferType.SPANNABLE);
        sectionBody.setMovementMethod(LinkMovementMethod.getInstance());
        return view;
    }

}
