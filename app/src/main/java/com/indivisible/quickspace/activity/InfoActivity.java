package com.indivisible.quickspace.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Window;
import com.indivisible.quickspace.R;
import com.indivisible.quickspace.fragment.InfoSectionFragment;
import com.indivisible.quickspace.fragment.TitlebarFragment;

/**
 * Created by indiv on 25/06/14.
 */
public class InfoActivity
        extends FragmentActivity
        implements TitlebarFragment.TitlebarButtonClickCallback
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private static final String TAG = "InfoActivity";


    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_info);

        TitlebarFragment titlebarFragment = TitlebarFragment.newInstance(false);


        String title;
        String body;

        // text for getString formatting args
        String indindWeb = getString(R.string.indivisible_HomeUrl);
        String clearMeOutApp = getString(R.string.indivisible_ClearMeOut_app);
        String quickSpaceWeb = getString(R.string.indivisible_QuickSpace_web);
        String quickSpaceRepo = getString(R.string.indivisible_QuickSpace_repo);

        String andChartsRepo = getString(R.string.AndroidChartsUrl);
        String iconspediaUrl = getString(R.string.IconspediaUrl);
        String doubleJUrl = getString(R.string.DoubleJIconUrl);

        // create fragments
        title = getString(R.string.info_this_title);
        body = getString(R.string.info_this_body_format, quickSpaceWeb, indindWeb);
        InfoSectionFragment appFrag = InfoSectionFragment.newInstance(title, body);

        title = getString(R.string.info_suggestion_title);
        body = getString(R.string.info_suggestion_clearmeout_format, clearMeOutApp);
        InfoSectionFragment suggestFrag = InfoSectionFragment.newInstance(title, body);

        title = getString(R.string.info_aboutme_title);
        body = getString(R.string.info_aboutme_body_format, indindWeb, quickSpaceRepo);
        InfoSectionFragment aboutMeFrag = InfoSectionFragment.newInstance(title, body);

        title = getString(R.string.info_attributions_title);
        body = getString(R.string.info_attributions_body,
                andChartsRepo,
                doubleJUrl,
                iconspediaUrl);
        InfoSectionFragment attributionFrag = InfoSectionFragment.newInstance(title, body);

        // add fragments
        FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                .beginTransaction();
        fragmentTransaction.add(R.id.titlebar, titlebarFragment);
        fragmentTransaction.add(R.id.info_activity_parent, appFrag);
        fragmentTransaction.add(R.id.info_activity_parent, suggestFrag);
        fragmentTransaction.add(R.id.info_activity_parent, aboutMeFrag);
        fragmentTransaction.add(R.id.info_activity_parent, attributionFrag);
        fragmentTransaction.commit();
    }


    ///////////////////////////////////////////////////////
    ////    communication / click handling (not used)
    ///////////////////////////////////////////////////////

    @Override
    public void onPrefsButtonClick()
    {}

    @Override
    public void onInfoButtonClick()
    {}
}
