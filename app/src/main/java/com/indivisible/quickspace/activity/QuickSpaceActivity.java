package com.indivisible.quickspace.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Window;
import android.widget.ImageButton;
import com.indivisible.quickspace.R;
import com.indivisible.quickspace.fragment.PartitionListFragment;
import com.indivisible.quickspace.fragment.TitlebarFragment;

public class QuickSpaceActivity
        extends FragmentActivity
        implements TitlebarFragment.TitlebarButtonClickCallback
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private ImageButton buttonPreferences;
    private ImageButton buttonInfo;

    private TitlebarFragment titlebarFragment;
    private PartitionListFragment partitionListFragment;

    private static final String TAG = "SizesDialogFrag";


    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_sizes);

        //        buttonInfo = (ImageButton) findViewById(R.id.title_button_info);
        //        buttonPreferences = (ImageButton) findViewById(R.id.title_button_preferences);
        //        buttonInfo.setOnClickListener(this);
        //        buttonPreferences.setOnClickListener(this);

        if (savedInstanceState == null)
        {
            Log.d(TAG, "(onCreate) bundle null, adding fragment...");
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            titlebarFragment = TitlebarFragment.newInstance(true);
            partitionListFragment = PartitionListFragment.newInstance();
            fragmentTransaction.add(R.id.titlebar, titlebarFragment);
            fragmentTransaction.add(R.id.dialog_fragment_partitionList, partitionListFragment);
            fragmentTransaction.commit();
        }
        else
        {
            Log.d(TAG, "(onCreate) bundle not null, restore previous state");
        }
    }


    ///////////////////////////////////////////////////////
    ////    button click handling
    ///////////////////////////////////////////////////////


    @Override
    public void onPrefsButtonClick()
    {
        Log.v(TAG, "(Click) Settings");
    }

    @Override
    public void onInfoButtonClick()
    {
        Log.v(TAG, "(Click) Info");
        startActivity(new Intent(this, InfoActivity.class));
    }
}
