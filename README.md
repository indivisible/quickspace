# QuickSpace #

An Android tool for showing you a quick overview of your storage partitions and their current levels

### Functionality ###

* Simple used/free space information
* PieChart to quickly see where you may need to free up space


### TODO ###

* Better and dynamic colouring of the PieChart view.
* Differentiation of the different partition types as all storage options are not created equal.
* User customisation
* Optional (configurable) notification for access anywhere and handy display of desired partition statuses
* Optional free space level monitoring/warning


### Note ###

A previous version of this app was developed with Eclipse/Ant and hosted on Github.  
To see that source (not being maintained any more) head to https://github.com/indivisible-irl/QuickSpace

I've since moved over to using AndroidStudio/Gradle and uploading to Bitbucket. I can't guarantee the Github repo will be there forever but I'm linking it just in case you are interested in seeing the older commit/history.
